﻿using CleanArchitectureApp.Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CleanArchitectureApp.Infrastructure.Persistence
{
    public class ApplicationDbContext: DbContext
    {

        public virtual DbSet<User> User { get; set; }


		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			modelBuilder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());
			/*SeedData(modelBuilder);*/

			base.OnModelCreating(modelBuilder);
		}

		/*private void SeedData(ModelBuilder modelBuilder)
		{
			ApplicationDbContextSeed.SeedStaticData(modelBuilder);
		}*/

		public IQueryable<TEnt> ReadSet<TEnt>() where TEnt : class
		{
			return Set<TEnt>().AsNoTracking();
		}

	/*	public IQueryable GetNonGenericSet(Type type)
		{
			return this.NonGenericSet(type);
		}*/

		public void EnsureDeleted()
		{
			Database.EnsureDeleted();
		}


		public async virtual Task<int> SaveChangesExAsync(CancellationToken token = default)
		{
			var allEntries = ChangeTracker.Entries().Where(e => e.Entity != null).ToList();

			try
			{
				Validate();
				/*AddAuditInformation(allEntries);*/

				return await SaveChangesAsync(token);
			}
			catch (Exception)
			{
				RejectChanges(allEntries);
				throw;
			}
		}

		public async Task<int> SaveChangesWithIdentityInsertAsync<TEnt>(CancellationToken token = default)
		{
			var strategy = Database.CreateExecutionStrategy();
			int ret = 0;

			await strategy.ExecuteAsync(async () =>
			{
				await using var transaction = await Database.BeginTransactionAsync(token);
				await SetIdentityInsertAsync<TEnt>(true, token);
				ret = await SaveChangesExAsync(token);
				await SetIdentityInsertAsync<TEnt>(false, token);
				await transaction.CommitAsync(token);
			});

			return ret;
		}

		public async Task ExecuteWithExplicitTransaction(Func<Task> operation, CancellationToken token)
		{
			var strategy = Database.CreateExecutionStrategy();

			await strategy.ExecuteAsync(async () =>
			{
				await using var transaction = await Database.BeginTransactionAsync(token);

				await operation();

				await transaction.CommitAsync(token);
			});
		}

		public DbSet<TEnt> RemoveRange<TEnt>(Expression<Func<TEnt, bool>> predicate) where TEnt : class
		{
			var dbSet = Set<TEnt>();
			var toDelete = dbSet.Where(predicate);
			dbSet.RemoveRange(toDelete);
			return dbSet;
		}

		//<inheritdoc/>
		public void RejectChanges(IList<EntityEntry> entries = null)
		{
			entries ??= ChangeTracker.Entries().Where(e => e.Entity != null).ToList();

			foreach (var entry in entries)
			{
				switch (entry.State)
				{
					case EntityState.Modified:
					case EntityState.Deleted:
						entry.State = EntityState.Modified; //Revert changes made to deleted entity.
						entry.State = EntityState.Unchanged;
						break;
					case EntityState.Added:
						entry.State = EntityState.Detached;
						break;
				}
			}
		}

		public void ForceModified<TEnt>(TEnt entity) where TEnt : class
		{
			var entry = Entry(entity);

			if (entry == null)
				return;

			entry.State = EntityState.Modified;
		}

		private async Task SetIdentityInsertAsync<TEnt>(bool enable, CancellationToken token)
		{
			var entityType = Model.FindEntityType(typeof(TEnt));
			string value = enable ? "ON" : "OFF";
			string query = $"SET IDENTITY_INSERT {entityType.GetSchema()}.{entityType.GetTableName()} {value}";
			await Database.ExecuteSqlRawAsync(query, token);
		}

		/// <summary>
		/// applies database context validations (annotations only) if any
		/// </summary>
		private void Validate()
		{
			// ReSharper disable once ComplexConditionExpression
			var entities = ChangeTracker.Entries()
				.Where(e => e.State == EntityState.Added || e.State == EntityState.Modified)
				.Select(e => e.Entity)
				.ToList();

			foreach (object entity in entities)
			{
				var validationContext = new ValidationContext(entity);
				Validator.ValidateObject(entity, validationContext, validateAllProperties: true);
			}
		}

	/*	private void AddAuditInformation(IList<EntityEntry> allEntries)
		{
			var changedEntries = allEntries
				.Where(e => e.State == EntityState.Added || e.State == EntityState.Modified)
				.ToList();

			foreach (var entity in changedEntries)
			{
				if (entity.State == EntityState.Added)
					HandleAdd(entity);

				if (entity.State == EntityState.Modified)
					HandleModified(entity);
			}
		}
*/
		/*private void HandleAdd(EntityEntry entry)
		{
			object entity = entry.Entity;
			var t = entity.GetType();
			bool isChron = typeof(IChronological).IsAssignableFrom(t);
			bool isUserContainer = typeof(IUserContainer).IsAssignableFrom(t);
			bool isOriginalUserContainer = typeof(IOriginalUserContainer).IsAssignableFrom(t);

			int currentUserId = _currentUserIdProvider.GetUserId();

			if (isChron)
				(entity as IChronological).DateCreation = _timeService.Now;

			if (isUserContainer)
				(entity as IUserContainer).User = currentUserId;

			if (isOriginalUserContainer)
				(entity as IOriginalUserContainer).OriginalUserId = currentUserId;
		}
*/
	/*	private void HandleModified(EntityEntry entry)
		{
			object entity = entry.Entity;
			var t = entity.GetType();
			bool isChron = typeof(IChronological).IsAssignableFrom(t);
			bool isUserContainer = typeof(IUserContainer).IsAssignableFrom(t);

			if (isChron)
				(entity as IChronological).DateModification = _timeService.Now;

			if (isUserContainer)
				(entity as IUserContainer).User = _currentUserIdProvider.GetUserId();
		}*/
	}
}
