﻿using System.Threading.Tasks;
using Amazon.SimpleSystemsManagement;
using Amazon.SimpleSystemsManagement.Model;
using CleanArchitectureApp.Application.Interfaces;
using CleanArchitectureApp.Application.Interfaces.Repositories;
using CleanArchitectureApp.Infrastructure.Persistence.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using NHibernate.Cfg;
using NHibernate.Cfg.MappingSchema;
using NHibernate.Dialect;
using NHibernate.Mapping.ByCode;

namespace CleanArchitectureApp.Infrastructure.Persistence
{
    public static class ServiceRegistration
    {
       /* public static IServiceCollection AddPersistenceInfrastructure(this IServiceCollection services, IConfiguration configuration)
        {
           // return services
              //  .AddSto
        }*/

        /*public static IServiceCollection AddCustomServices(this IServiceCollection services)
        {

        }*/

        public static IServiceCollection AddStorage(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<ApplicationDbContext>(options => options
            .EnableDetailedErrors()
            .EnableSensitiveDataLogging()
            .UseSqlServer(configuration.GetConnectionString("DefaultConnection")));


            services.AddScoped<ApplicationDbContext>();

            return services;
        }
    }
}