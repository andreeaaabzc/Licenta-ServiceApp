using System;
using System.Collections.Generic;

namespace CleanArchitectureApp.Domain
{
    public class User
    {
        public User()
        {
            UserTokens = new List<UserToken>();
        }

        public int UserId { get; set; }
        public virtual UserStatuses UserStatuses { get; set; }
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserEmail { get; set; }
        public string PasswordHash { get; set; }
        public  string PasswordSalt { get; set; }
        public string Phone { get; set; }
        public int UpdatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public DateTime CreatedDate { get; set; }
        public IList<UserToken> UserTokens { get; set; }

    }
}