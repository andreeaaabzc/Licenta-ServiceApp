namespace CleanArchitectureApp.Domain
{
    public partial class LoginLogType
    {
        public int LoginLogTypeId { get; set; }
        public string LoginLogTypeName { get; set; }
    }
}