﻿using System;

namespace CleanArchitectureApp.Domain
{
    public abstract class AuditableBaseEntity
    {
        public int CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public int UpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
    }
}