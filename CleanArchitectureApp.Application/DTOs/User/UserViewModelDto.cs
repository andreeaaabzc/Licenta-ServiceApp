using System;

namespace CleanArchitectureApp.Application.DTOs.User
{
    public class UserViewModelDto
    {
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserEmail { get; set; }
        public string PasswordHash { get; set; }
        public string Phone { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}