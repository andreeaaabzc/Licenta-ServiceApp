﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.EntityFrameworkCore.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CleanArchitectureApp.Application.Interfaces
{
    public interface IApplicationDbContext
    {
        /// <summary>
        /// db set
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        DbSet<TEntity> Set<TEntity>() where TEntity : class;

		/// <summary>
		/// a read-only reference to a dbset
		/// </summary>
		/// <typeparam name="TEnt"></typeparam>
		/// <returns></returns>
		IQueryable<TEnt> ReadSet<TEnt>() where TEnt : class;

		/// <summary>
		/// gets a DbSet in a non-generic way
		/// </summary>
		/// <param name="type"></param>
		/// <returns></returns>
		IQueryable GetNonGenericSet(Type type);

		/// <summary>
		/// save changes by wrapping custom functionality
		/// </summary>
		/// <param name="cancellationToken"></param>
		/// <returns></returns>
		Task<int> SaveChangesExAsync(CancellationToken cancellationToken = default);

		/// <summary>
		/// save changes with enabling/disabling of the identity insert for a certain type
		/// </summary>
		/// <typeparam name="TEnt"></typeparam>
		/// <param name="token"></param>
		/// <returns></returns>
		Task<int> SaveChangesWithIdentityInsertAsync<TEnt>(CancellationToken token = default);

		/// <summary>
		/// allows execution with explicit transaction control
		/// this is required because the default strategy does not allow explicit transactions
		/// </summary>
		/// <param name="operation"></param>
		/// <param name="token"></param>
		/// <returns></returns>
		Task ExecuteWithExplicitTransaction(Func<Task> operation, CancellationToken token);

		/// <summary>
		/// removes all data from the database
		/// </summary>
		void EnsureDeleted();

		/// <summary>
		/// removes all entities from a DbSet based on a predicate
		/// </summary>
		/// <typeparam name="TEnt"></typeparam>
		/// <param name="predicate"></param>
		/// <returns></returns>
		DbSet<TEnt> RemoveRange<TEnt>(Expression<Func<TEnt, bool>> predicate) where TEnt : class;

		/// <summary>
		/// adds a list of objects
		/// </summary>
		/// <param name="entities"></param>
		void AddRange(IEnumerable<object> entities);

		/// <summary>
		/// adds a variable number of entities
		/// </summary>
		/// <param name="entities"></param>
		void AddRange(params object[] entities);

		/// <summary>
		/// 
		/// </summary>
		/// <typeparam name="TEntity"></typeparam>
		/// <param name="entity"></param>
		/// <returns></returns>
		EntityEntry<TEntity> Update<TEntity>(TEntity entity) where TEntity : class;

		/// <summary>
		/// database reference
		/// </summary>
		DatabaseFacade Database { get; }


		/// <summary>
		/// change tracker reference
		/// </summary>
		ChangeTracker ChangeTracker { get; }

		/// <summary>
		/// reverts all changes done in the current context
		/// </summary>
		/// <param name="entries"></param>
		/// <remarks>Should be used only when something needs to be saved in the database on failure. 
		/// Save changes takes care to automatically call revert on failure to ensure a clean context</remarks>
		void RejectChanges(IList<EntityEntry> entries = null);

		/// <summary>
		/// force change the state of the entity to modified
		/// </summary>
		/// <typeparam name="TEnt"></typeparam>
		/// <param name="entity"></param>
		public void ForceModified<TEnt>(TEnt entity) where TEnt : class;
	}
}
